; ---------------------------------------------------------------------
; TONTON Drupal 7 Full make file
; @author opi
;
; "Full"  means base + usefull modules
;
; INSTRUCTIONS
; drush make PATH/TO/d7_base.make  SITE_DIRECTORY --force-complete
; ---------------------------------------------------------------------


includes[] = 


; -------
; Modules
; -------

; Base modules
projects[mollom][subdir] = "contrib"

; Entity
projects[entity][subdir] = "contrib"
projects[entityreference][subdir] = "contrib"

; Fields
projects[field_collection][subdir] = "contrib"
projects[field_group][subdir] = "contrib"

; SEO
projects[metatag][subdir] = "contrib"

; Ctools suite
projects[ctools][subdir] = "contrib"
projects[views][subdir] = "contrib"
projects[context][subdir] = "contrib"

; User Interface
projects[colorbox][subdir] = "contrib"
projects[wysiwyg][subdir] = "contrib"
projects[imce][subdir] = "contrib"
projects[imce_wysiwyg][subdir] = "contrib"

; Libraries
projects[libraries][subdir] = "contrib"


; ---------
; Libraries
; ---------

libraries[tinymce][download][type] = "file"
libraries[tinymce][download][url] = "http://github.com/downloads/tinymce/tinymce/tinymce_3.4.5.zip"




