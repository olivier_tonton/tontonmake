Tonton Drush Make Files
=======================

d7_base.make
------------

drush make https://bitbucket.org/olivier_tonton/tontonmake/raw/4cf6cd2ed583/d7_base.make SITE_DIRECTORY

( SITE_DIRECTORY will be created by drush make )



d7_full.make
------------

drush make https://bitbucket.org/olivier_tonton/tontonmake/raw/8791bd9e7f6e/d7_full.make SITE_DIRECTORY

( SITE_DIRECTORY will be created by drush make )
