; ---------------------------------------------------------------------
; TONTON Drupal 7 base make file
; @author opi
;
; INSTRUCTIONS
; drush make PATH/TO/d7_base.make  SITE_DIRECTORY --force-complete
; ---------------------------------------------------------------------


; -----------
; API version
; -----------

api = 2


; ----
; Core
; ----

core = 7.x
projects[drupal][version] = 7  


; -------
; Modules
; -------

; Base modules
projects[token][subdir] = "contrib"
projects[transliteration][subdir] = "contrib"

; Admin modules
projects[backup_migrate][subdir] = "contrib"
projects[admin_menu][subdir] = "contrib"

; SEO
projects[pathauto][subdir] = "contrib"
projects[google_analytics][subdir] = "contrib"

; User Interface
projects[l10n_update][subdir] = "contrib"

; HTML5 Support
projects[elements][subdir] = "contrib"
projects[html5_tools][subdir] = "contrib"

; Development
projects[devel][subdir] = "contrib"


;-------
; Themes
; ------

; Admin themes, we love Rubik !
projects[tao][subdir] = "contrib"
projects[rubik][subdir] = "contrib"


; ---------
; Libraries
; ---------


